package com.sda.brigi.invoiceadvanced.model;
//Create a class called Invoice that a hardware store might use to represent an invoice for an item sold at the store.

//An Invoice should include four pieces of information as instance variables‐a part number(type String),
//a part description(type String),a quantity of the item being purchased (type int) 
//and a price per item  (double). Your class should have a constructor that initializes the four instance variables. 
//Provide a set and a get method for each instance variable. 
//In addition, provide a method named getInvoiceAmount() that calculates the invoice amount (i.e., multiplies the quantity by the price per item),
//then returns the amount as a double value.

public class Invoice {
	private String partNumber;
	private String partDescription;
	private int itemPurchased;
	private double pricePerItem;

	public Invoice(String partNumber, String partDescription, int itemPurchased, double pricePerItem) {
		this.partNumber = partNumber;
		this.partDescription = partDescription;
		this.itemPurchased = itemPurchased;
		this.pricePerItem = pricePerItem;
	}

	public String getPartNumber() {
		return partNumber;
	}

	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}

	public String getPartDescription() {
		return partDescription;
	}

	public void setPartDescription() {
		this.partDescription = partDescription;
	}

	public int getItemPurchased() {
		return itemPurchased;
	}

	public void setItemPurchased() {
		this.itemPurchased = itemPurchased;

	}

	public double getPricePerItem() {
		return pricePerItem;
	}

	public void setPricePerItem(double pricePerItem) {
		this.pricePerItem = pricePerItem;
	}

	public double getInvoiceAmount() {
		//this.itemPurchased=itemPurchased;
		//this.pricePerItem=pricePerItem;
		double amount = itemPurchased * pricePerItem;
		return amount;
	}

	@Override
	public String toString() {
		return "Invoice [partNumber=" + partNumber + ", partDescription=" + partDescription + ", itemPurchased="
				+ itemPurchased + ", pricePerItem=" + pricePerItem + "]";
	}
	
	
}
