package com.sda.brigi.invoiceadvance.main;

import java.util.ArrayList;
import java.util.List;

import com.sda.brigi.invoiceadvanced.model.Invoice;

public class InvoiceMain {

	public static void main(String[] args) {

		Invoice invoice1 = new Invoice("2", "hardware", 2, 1.5);
		Invoice invoice2 = new Invoice("3", "hardware2", 3, 2.5);
		Invoice invoice3 = new Invoice("4", "hardware3", 4, 3.5);

		System.out.println(invoice1.getItemPurchased());
		System.out.println(invoice1.getPartDescription());
		System.out.println(invoice1.getPartNumber());
		System.out.println(invoice1.getPricePerItem());
		System.out.println("Invoice1 amount :" + invoice1.getInvoiceAmount());
		System.out.println("Invoice2 amount :" + invoice2.getInvoiceAmount());
		System.out.println("Invoice3 amount :" + invoice3.getInvoiceAmount());
		// double
		// sumOfInvoice=invoice1.getInvoiceAmount()+invoice2.getInvoiceAmount()+invoice3.getInvoiceAmount();

		List<Invoice> invoicelist = new ArrayList<>();// la list is arraylist trebuie sa facem import
		invoicelist.add(invoice1);
		invoicelist.add(invoice2);
		invoicelist.add(invoice3);

		double sumOfInvoice = 0;
		for (Invoice invoice : invoicelist) {
			sumOfInvoice = sumOfInvoice + invoice.getInvoiceAmount();
		}

		System.out.println("Sum Of invoices: " +sumOfInvoice);

		// System.out.println(sumOfInvoice);

		// sau
		double sum = 0;
		 for (int i=0; i<invoicelist.size(); i++) {
		sum=sum+invoicelist.get(i).getInvoiceAmount();
		 }
		System.out.println("Sum Of invoices: "+ sum);
		// System.out.println(invoicelist.get(i));
	}
}
